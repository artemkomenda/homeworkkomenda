class Employee{
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value){
        this._name = value;
    }

    get name(){
        return this._name;
    }

    set age(value){
        this._age = value;
    }
    get age(){
        return this._age;
    }

    set salary(value){
        this._salary = value;
    }
    get salary(){
        return this._salary;
    }
}

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this.lang = lang
    }

    set salary(value){
        this._salary = value * 3
    }
    get salary(){
        return this._salary * 3
    }
}

const employee  = new Employee("Artem",17,1200,"us")
console.log(employee)

const programmer = new Programmer("Max",20,4000,"ua") 
console.log(programmer)